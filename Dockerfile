FROM python:3.7.7-alpine as app

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "./server.py" ]

FROM nginx:1.15-alpine as proxy

COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY ./docker-entrypoint-nginx.sh /docker-entrypoint.sh
RUN chmod 777 /docker-entrypoint.sh

ENV NGINX_PORT=80
ENV UPSTREAM_HOST=localhost
ENV UPSTREAM_PORT=8000

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
