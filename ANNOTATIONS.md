# Solution description

Provided solution allows Lyte application to be scaled and fault-tolerant

## Docker

- Multi-stage Dockerfile is used to keep Docker config in one place
- Nginx configuration template is added to Nginx Docker image
- Nginx container populates and copy Nginx configuration based on environment variables at container startup

## AWS infrastructure

- AWS ECS was choosen to simplify container deployment and management
- AWS ECR used as Docker image registry
- AWS VPC splitted onto private and public subnets distributed across different Availability Zones for redundancy
- Each of private subnet has it's own AWS NAT gateway to access Internet
- EC2 instance for serving Docker containers launched in AWS Autoscaling Group
- AWS ALB placed in public subnet and distributes all incoming connections across application instances (ECS tasks)
- AWS EC2 instance in Autoscaling Group joins the ECS cluster based on configuration, provided via User Data
- All the logs from each Docker container gets to the `lyte-ecs` AWS Cloudwatch group
- Health check endpoint for AWS ALB is added to application (`/status`)

## Pipeline

- Deployment pipeline is triggered when Git tag is pushed to the repository
- Authorisation for Docker image registry is required and gets executed
- Gitlabs tries to pull images to utilize Docker layer caching on build phase
- Built Docker images are pushed to the AWS ECR
- ECS Task definition template `ecs/taskdef.tpl` gets populated with Docker image name and tag
- ECS Task definition is registered in ECS
- ECS Service template gets populated with ECS Task definition revision
- ECS Service gets updated or created if it does not exist yet

## Deployment

### Requirements

- AWS Access key and secret as well as AWS region must be provided via environment variables (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_DEFAULT_REGION)
- Recommended IAM policy for AWS IAM User is `PowerUserAccess` one managed by AWS

### Steps

1. AWS infrastructure must be created before launching the deployment pipeline. To create application infrastructure from scratch run:
```
terraform init
terraform apply
```

Capture Terraform outputs for the next step

2. Populate following environment variables for Gitlab CI/CD using values from previous step:
   - AWS_DEFAULT_REGION
   - AWS_ACCESS_KEY_ID
   - AWS_SECRET_ACCESS_KEY
   - CI_APP_IMAGE_REPO
   - CI_PROXY_IMAGE_REPO

3. Push Git tag to the repository to trigger deployment pipeline

## Local development

To launch application stack in locally simply run:
```
docker-compose up
```
By default Nginx listens on port tcp/80. To change it it needs to change `NGINX_PORT` environment variable for Nginx container in `docker-compose.yml` and adjust port mapping accordingly.

## Remarks

- Infrastruction can be launched in any AWS account from scratch
- Community Terraform modules (such as VPC, ECS cluster etc) was not used intentionally to demonstrate knowledges and skills
- AWS API keys for deployment added to the Terraform state to make the environment be easily unfolded
- The same reason for using Terraform local backend
- Terraform state isn't commited to avoid credentials leaking
