{
  "cluster": "lyte", 
  "serviceName": "webapp", 
  "taskDefinition": "lyte-webapp:${ECS_TASKDEF_REV}", 
  "desiredCount": 2,
  "loadBalancers": [
    {
      "targetGroupArn": "arn:aws:elasticloadbalancing:eu-central-1:395802769230:targetgroup/lyte-app/78298363ea001052",
      "containerName": "proxy",
      "containerPort": 80
    }
  ]
}
