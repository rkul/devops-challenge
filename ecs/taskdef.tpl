{
  "containerDefinitions": [
    {
      "memoryReservation": 128,
      "environment": [
        {
          "name": "PYTHONUNBUFFERED",
          "value": "1"
        },
        {
          "name": "APP_PORT",
          "value": "8000"
        }
      ],
      "essential": true,
      "image": "${CI_APP_IMAGE_REPO}:${CI_COMMIT_TAG}",
      "name": "app",
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-region": "eu-central-1",
          "awslogs-group": "lyte-ecs",
          "awslogs-stream-prefix": "webapp"
        }
      }
    },
    {
      "memoryReservation": 128,
      "environment": [
        {
          "name": "NGINX_PORT",
          "value": "80"
        },
        {
          "name": "UPSTREAM_PORT",
          "value": "8000"
        },
        {
          "name": "UPSTREAM_HOST",
          "value": "app"
        }
      ],
      "essential": true,
      "image": "${CI_PROXY_IMAGE_REPO}:${CI_COMMIT_TAG}",
      "name": "proxy",
      "links": [
        "app"
      ],
      "portMappings": [
        {
          "containerPort": 80
        }
      ],
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-region": "eu-central-1",
          "awslogs-group": "lyte-ecs",
          "awslogs-stream-prefix": "webapp"
        }
      }
    }
  ],
  "family": "lyte-webapp"
}
