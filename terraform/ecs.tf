resource "aws_ecs_cluster" "lyte" {
  name = "lyte"
}

data "aws_ami" "ecs" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-ecs-hvm-*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

locals {
  user_data = <<-EOT
    #!/bin/bash
    echo ECS_CLUSTER=${aws_ecs_cluster.lyte.name} >> /etc/ecs/ecs.config
  EOT
}
resource "aws_launch_template" "ecs_lyte" {
  block_device_mappings {
    device_name = data.aws_ami.ecs.*.root_device_name[0]

    ebs {
      volume_type = "gp2"
      volume_size = "30"
    }
  }

  iam_instance_profile {
    name = aws_iam_instance_profile.ecs_instance.name
  }

  image_id               = data.aws_ami.ecs.*.image_id[0]
  instance_type          = var.container_instance_type
  name_prefix            = "lyte-"
  user_data              = base64encode(local.user_data)
  vpc_security_group_ids = [aws_security_group.lyte_ecs.id]
}

resource "aws_autoscaling_group" "ecs_lyte" {
  desired_capacity          = 1
  health_check_grace_period = "180"
  health_check_type         = "EC2"
  max_size                  = 1
  min_size                  = 1
  name_prefix               = "lyte-"
  termination_policies      = ["OldestLaunchConfiguration", "Default"]
  wait_for_capacity_timeout = 0
  vpc_zone_identifier       = aws_subnet.lyte_private.*.id

  launch_template {
    id      = aws_launch_template.ecs_lyte.id
    version = "$Latest"
  }

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "lyte-ecs"
    propagate_at_launch = true
  }
}
