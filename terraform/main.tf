terraform {
  required_version = "~> 0.12"

  required_providers {
    aws = "~> 2.56"
  }
}

provider "aws" {
  region = "eu-central-1"
}

resource "aws_vpc" "lyte" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "lyte"
  }
}

resource "aws_subnet" "lyte_private" {
  count             = length(var.private_subnet_list)
  vpc_id            = aws_vpc.lyte.id
  availability_zone = var.az_list[count.index]
  cidr_block        = var.private_subnet_list[count.index]

  tags = {
    Name = "lyte_private"
  }
}

resource "aws_subnet" "lyte_public" {
  count             = length(var.public_subnet_list)
  availability_zone = var.az_list[count.index]
  vpc_id            = aws_vpc.lyte.id
  cidr_block        = var.public_subnet_list[count.index]

  tags = {
    Name = "lyte_public"
  }
}

resource "aws_internet_gateway" "lyte" {
  vpc_id = aws_vpc.lyte.id
}

resource "aws_eip" "lyte_nat" {
  count = length(var.private_subnet_list)
  vpc   = true
}

resource "aws_nat_gateway" "lyte_nat" {
  count         = length(var.private_subnet_list)
  allocation_id = element(aws_eip.lyte_nat.*.id, count.index)
  subnet_id     = element(aws_subnet.lyte_public.*.id, count.index)
}

resource "aws_route_table" "lyte_public" {
  vpc_id = aws_vpc.lyte.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.lyte.id
  }

  tags = {
    Name = "lyte-public"
  }
}

resource "aws_route_table" "lyte_private" {
  count  = length(var.private_subnet_list)
  vpc_id = aws_vpc.lyte.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.lyte_nat.*.id[count.index]
  }

  tags = {
    Name = "lyte-private"
  }
}

resource "aws_route_table_association" "public" {
  count          = length(var.public_subnet_list)
  subnet_id      = aws_subnet.lyte_public.*.id[count.index]
  route_table_id = aws_route_table.lyte_public.id
}

resource "aws_route_table_association" "private" {
  count          = length(var.private_subnet_list)
  subnet_id      = aws_subnet.lyte_private.*.id[count.index]
  route_table_id = aws_route_table.lyte_private.*.id[count.index]
}
