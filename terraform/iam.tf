data "aws_iam_policy_document" "ec2_assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "ecs_instance" {
  name               = "ecs_instance"
  assume_role_policy = data.aws_iam_policy_document.ec2_assume_role.json
}

resource "aws_iam_role_policy_attachment" "ecs_instance_container_service" {
  role       = aws_iam_role.ecs_instance.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_role_policy_attachment" "ecs_instance_ecr" {
  role       = aws_iam_role.ecs_instance.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

resource "aws_iam_instance_profile" "ecs_instance" {
  name = "ecs_instance"
  role = aws_iam_role.ecs_instance.name
}

resource "aws_iam_user" "deployer" {
  name = "deployer"
}

resource "aws_iam_access_key" "deployer" {
  user = aws_iam_user.deployer.name
}

resource "aws_iam_user_policy_attachment" "deployer_ecr" {
  user       = aws_iam_user.deployer.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser"
}

resource "aws_iam_user_policy" "ecs_deployer" {
  name = "CustomECSDeployer"
  user = aws_iam_user.deployer.name

  policy = <<-EOT
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": [
            "ecs:CreateService",
            "ecs:DeregisterTaskDefinition",
            "ecs:DescribeServices",
            "ecs:DescribeTaskDefinition",
            "ecs:DescribeTasks",
            "ecs:ListTasks",
            "ecs:ListTaskDefinitions",
            "ecs:RegisterTaskDefinition",
            "ecs:StartTask",
            "ecs:StopTask",
            "ecs:UpdateService",
            "iam:PassRole"
          ],
          "Resource": "*"
        }
      ]
    }
  EOT
}

output "deployer_aws_access_key_id" {
  value = aws_iam_access_key.deployer.id
}
output "deployer_secret_access_key" {
  value = aws_iam_access_key.deployer.secret
}
