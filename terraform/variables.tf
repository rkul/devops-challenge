variable "vpc_cidr" {
  default = "10.10.0.0/16"
}

variable "az_list" {
  default = ["eu-central-1a", "eu-central-1b"]
}

variable "private_subnet_list" {
  default = ["10.10.10.0/24", "10.10.11.0/24"]
}

variable "public_subnet_list" {
  default = ["10.10.0.0/24", "10.10.1.0/24"]
}

variable "container_instance_type" {
  default = "t2.micro"
}
