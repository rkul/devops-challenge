resource "aws_lb" "lyte_ecs" {
  name               = "lyte-ecs"
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lyte_lb.id]
  subnets            = aws_subnet.lyte_public.*.id

  tags = {
    Name = "lyte-ecs"
  }
}

resource "aws_lb_target_group" "lyte_app" {
  name                 = "lyte-app"
  port                 = "80"
  protocol             = "HTTP"
  deregistration_delay = 5
  vpc_id               = aws_vpc.lyte.id
  stickiness {
    type            = "lb_cookie"
    cookie_duration = 86400 # 1 day
    enabled         = true
  }

  health_check {
    path     = "/status"
    protocol = "HTTP"
    port     = "traffic-port"
  }
}

resource "aws_lb_listener" "lyte_ecs_80" {
  load_balancer_arn = aws_lb.lyte_ecs.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lyte_app.arn
  }
}

output "alb_url" {
  value = "http://${aws_lb.lyte_ecs.dns_name}"
}

output "alb_target_group_arn" {
  value = aws_lb_target_group.lyte_app.arn
}
