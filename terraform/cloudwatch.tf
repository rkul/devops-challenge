resource "aws_cloudwatch_log_group" "lyte_ecs" {
  name              = "lyte-ecs"
  retention_in_days = 7
}
