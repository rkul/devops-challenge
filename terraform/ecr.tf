resource "aws_ecr_repository" "app" {
  name = "app"
}

resource "aws_ecr_repository" "proxy" {
  name = "proxy"
}

output "app_ecr_repo" {
  value = aws_ecr_repository.app.repository_url
}

output "proxy_ecr_repo" {
  value = aws_ecr_repository.proxy.repository_url
}
