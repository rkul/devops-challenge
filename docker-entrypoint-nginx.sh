#!/bin/sh

set -e

VARS=`env | cut -d= -f1 | sed -e 's/^/\$/' | paste -s -d , -`
echo "$(envsubst $VARS < /etc/nginx/conf.d/default.conf)" > /etc/nginx/conf.d/default.conf
exec "$@"
